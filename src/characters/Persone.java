package characters;

public abstract class Persone {
    String name;
    int hp;
    int pow;
    public Persone(String name, int hp, int pow) {
        this.name = name;
        this.hp = hp;
        this.pow = pow;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public void setPow(int pow) {
        this.pow = pow;
    }

    public String getName() {
        return name;
    }

    public int getHp() {
        return hp;
    }

    public int getPow() {
        return pow;
    }
}
