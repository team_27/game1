package mvc;

import characters.Elf;
import characters.MainPersone;
import characters.Persone;
import characters.Monster;

import java.util.Scanner;

public class Application {
    public static void main(String[] args) {
        Controller c = new Controller(); //Ото переробити інфо 100%
        System.out.println("Введите Ваше имя: ");
        Scanner sc = new Scanner(System.in);
        MainPersone hero = new Elf(sc.nextLine());
        Monster monster = c.getRandomMonster();
        while(hero.getHp() > 0){
            c.getRandomMonster();
            System.out.println("Вы сражаетесь с " + monster.monsterName);
            c.fight(monster, hero);
        }
        System.out.println("GG");
    }
}
